local ipaddr=$(/bin/ip addr | grep 'eth0\|br0\|bond0\|venet0\|en\w*[os]\|host0' | grep -v "127\.0\.\|192\.168\." | grep "inet " | head -n 1 | awk '{print $2}' | awk -F/ '{print $1}')
local ip='%{$fg[yellow]%}${ipaddr}%{$reset_color%}'
local time='%{$fg[blue]%}%*%{$reset_color%}'
local user='%{$fg[green]%}%n%{$reset_color%}@%{$fg[magenta]%}%m%{$reset_color%}'
local pwd='%{$fg[red]%}%~%{$reset_color%}'

if [[ $EUID = 0 ]]; then
    local prompt_sign="#"
else
    local prompt_sign="$"
fi

PROMPT="${ip} ${time} ${user}:${pwd}${prompt_sign} "
