home = Dir.home
omz = "#{home}/.oh-my-zsh"
etc = "#{home}/.etc"

task :default => [:omz, :config]
task :config => [:omz_hl, :omz_theme, :htop, :zshrc, :iftoprc, :chsh]
task :omz => "#{omz}/oh-my-zsh.sh"
task :omz_hl => "#{omz}/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
task :omz_theme => "#{omz}/custom/themes/msg7086.zsh-theme"
task :htop => "#{home}/.config/htop/htoprc"
task :zshrc => "#{home}/.zshrc"
task :iftoprc => "#{home}/.iftoprc"

file "#{omz}/oh-my-zsh.sh" do
  rm_rf omz if File.exist? omz
  sh "git clone --depth=1 https://github.com/robbyrussell/oh-my-zsh.git #{omz}"
end

file "#{omz}/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" => [omz] do
  mkdir_p "#{omz}/custom/plugins"
  sh "git clone --depth=1 https://github.com/zsh-users/zsh-syntax-highlighting.git #{omz}/custom/plugins/zsh-syntax-highlighting"
end

file "#{omz}/custom/themes/msg7086.zsh-theme" => ["#{etc}/config/msg7086.zsh-theme", omz] do |t|
  mkdir_p File.dirname(t.name)
  cp t.source, t.name
end

file "#{home}/.config/htop/htoprc" => ["#{etc}/config/htoprc"] do |t|
  mkdir_p File.dirname(t.name)
  cp t.source, t.name
end

file "#{home}/.zshrc" => ["#{etc}/config/zshrc"] do |t|
  cp t.name, "#{t.name}-#{Time.now.to_i}" if File.exist? t.name
  cp t.source, t.name
end

file "#{home}/.iftoprc" do |t|
  File.write(t.name, 'max-bandwidth: 50M')
end

task :chsh do
  user = File.readlines('/etc/passwd').select{|l| l[ENV['USER']]}.first
  sh 'chsh -s /bin/zsh' if !user['zsh']
end
